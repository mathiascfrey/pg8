$(function() {

    
    
    //--------------------------------------------------------------------------
    // init affix    
    $('#myAffix').affix({
       offset: {
         top: function() {
           return ($('#start_affix_contentarea').offset().top)
         }
         , bottom: function () {
           return (this.bottom = $('#call2action').outerHeight(true))
         }
       }
     })




    //--------------------------------------------------------------------------
    // nice scrolling
    //$("ul.nav li a[href^='#']").on('click', function(e) {
    $(".innerside-nav").on('click', function(e) {
        
       target = this.hash;
       // prevent default anchor click behavior
       e.preventDefault();
    
       // store hash
       var hash = this.hash;
    
       // animate
       $('html, body').animate({
           scrollTop: $(this.hash).offset().top
         }, 600, function(){
    
           // when done, add hash to url
           // (default click behaviour)
           window.location.hash = target;
         });
    
    });
    //--------------------------------------------------------------------------
    
   //--------------------------------------------------------------------------

});



//-------------------------------- e0efef  353e48
// GOOGLE MAPS
//
function initialize() {
  var mapLatlng = new google.maps.LatLng(48.202,16.3630781);
  var myLatlng = new google.maps.LatLng(48.1960195,16.3667538);
  var mapOptions = {
    zoom: 15,
    disableDefaultUI: true,
    center: mapLatlng,
    styles: [{"featureType":"water","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]},
             {"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},
             {"featureType":"road","elementType":"geometry","stylers":[{"color":"#e0efef"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},
             {"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-80},{"lightness":60},{"visibility":"simplified"},{"hue":"#353e48"}]},
            // {"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},
             {"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},
             {"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},
             {"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},
             {"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]}]
    
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  var contentString = '<div id="mapcontent">' +
  '<h1>pg8</h1>' +
  '</div>';
  var infowindow = new google.maps.InfoWindow({
      content: contentString
  });

   
  //var image = 'img/Logo_c99_small_white.png';
  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'office'
      //icon: image
  });
  infowindow.open(map,marker);
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
      'callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;